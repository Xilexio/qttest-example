Example of a Qt project using QtTest library. Check out http://xilexio.org for a tutorial connected with it.

Public Domain license. You may do with this code as you please.

Author: Xilexio <xilexio@xilexio.org>