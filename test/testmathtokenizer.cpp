#include "testmathtokenizer.h"
#include "mathtokenizer.h"

void TestMathTokenizer::initTestCase() {
    // This function is being executed at the beginning of each test suite
    // That is - before other tests from this class run
    qDebug() << "TestMathTokenizer::initTestCase()";
}

void TestMathTokenizer::cleanupTestCase() {
    // Similarly to initTestCase(), this function is executed at the end of test suite
    qDebug() << "TestMathTokenizer::cleanupTestCase()";
}

void TestMathTokenizer::init() {
    // This function is executed before each test
    qDebug() << "TestMathTokenizer::init()";
}

void TestMathTokenizer::cleanup() {
    // This function is executed after each test
    qDebug() << "TestMathTokenizer::cleanup()";
}

void TestMathTokenizer::testEmpty() {
    qDebug() << "Inside TestMathTokenizer::testEmpty()";

    QString error;
    MathTokenizer tokenizer;
    QList<Token> tokens = tokenizer.tokenize("", error);

    // QVERIFY2 is for assertion with a comment and QVERIFY for assertion without one
    QVERIFY2(tokens.isEmpty(), "Tokens list should be empty");
    QVERIFY2(error.isEmpty(), "Error string should be empty");
}

void TestMathTokenizer::testInt() {
    QString error;
    MathTokenizer tokenizer;
    QList<Token> tokens = tokenizer.tokenize("123", error);

    // We can compare data using QCOMPARE (only without a comment)
    QCOMPARE(tokens.count(), 1);
    // We can compare in the traditional way too
    QVERIFY(tokens.first().type() == Token::IntegerToken);
    // Notice we can compare QString with const char* here, because of automatic conversion
    QVERIFY(tokens.first().content() == "123");
    QVERIFY(error.isEmpty());
}

void TestMathTokenizer::testIntFail() {
    QString error;
    MathTokenizer tokenizer;
    tokenizer.tokenize("000123", error);

    QVERIFY(!error.isEmpty());
}

void TestMathTokenizer::testExpr() {
    QString error;
    MathTokenizer tokenizer;
    QList<Token> tokens = tokenizer.tokenize("1+1-1*1/1", error);

    QCOMPARE(tokens.count(), 9);
    QCOMPARE(tokens.at(3).type(), Token::OperatorToken);
    // We can't compare QString with a const char* with QCOMPARE because of conversion problems
    QCOMPARE(tokens.at(3).content(), QString("-"));
    QCOMPARE(tokens.at(4).type(), Token::IntegerToken);
    QVERIFY(error.isEmpty());
}

void TestMathTokenizer::testUnexpectedCharacterFail() {
    QString error;
    MathTokenizer tokenizer;
    tokenizer.tokenize("1 + 1", error);

    QVERIFY2(!error.isEmpty(), "Spaces should not be allowed");
}

void TestMathTokenizer::testFutureFunctionality() {
    QString error;
    MathTokenizer tokenizer;
    tokenizer.tokenize("sqrt(4)", error);

    QEXPECT_FAIL("", "sqrt not implemented yet", Abort);
    QVERIFY(error.isEmpty());
}

void TestMathTokenizer::testZero() {
    QString error;
    MathTokenizer tokenizer;
    tokenizer.tokenize("0", error);

    QSKIP("This assertion is boring, so skipping it", SkipSingle);
    QVERIFY(false);

    QVERIFY(error.isEmpty());
}

void TestMathTokenizer::evilFunction() {
    Q_ASSERT("This function is evil, so it shouldn't be ran");
}
