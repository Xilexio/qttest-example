#ifndef TESTMATHTOKENIZER_H
#define TESTMATHTOKENIZER_H

#include <QtTest>

// Test suite for MathTokenizer
// Contains lots of additional (not really needed) elements to show some of QtTest functionalities
class TestMathTokenizer : public QObject {
    Q_OBJECT

private slots:
    // functions executed by QtTest before and after test suite
    void initTestCase();
    void cleanupTestCase();

    // functions executed by QtTest before and after each test
    void init();
    void cleanup();

    // test functions - all functions prefixed with "test" will be ran as tests
    // this is automatically detected thanks to Qt's meta-information about QObjects
    void testEmpty();
    void testInt();
    void testIntFail();
    void testExpr();
    void testUnexpectedCharacterFail();
    void testFutureFunctionality();
    void testZero();

    // this function is not prefixed with "test", so it won't be ran as one
    void evilFunction();
};

#endif // TESTMATHTOKENIZER_H
